import React from 'react';
import {
  Platform,
  Text,
  TextInput,
  View,
  Card,
  Button,
  StyleSheet,
  AsyncStorage,
  Image,
  TouchableHighlight,
  ListView,
  ScrollView,
  Alert,
  ImagePickerIOS,
  Dimensions
} from 'react-native';
import {ImagePicker, DocumentPicker, Video} from 'expo';
import Thumbnail from 'react-native-thumbnail-video';
import LinkPreview from 'react-native-link-preview';

const VIDEO = 'https://www.youtube.com/watch?v=lgj3D5-jJ74';
const TYPING_TIMEOUT = 700;


//import {Ionicons} from '@expo/vector-icons';

//import FileUploader from 'react-native-file-uploader'
//import FileUpload from 'react-native-upload-file'
import {FontAwesome} from '@expo/vector-icons';




let Icon = FontAwesome;

const loginURL = "https://ginger.activecloudediting.com/back/user/api-token-auth/";
const projectsListURL = "https://ginger.activecloudediting.com/back/ace/api/projects/";
const uploadMediaURL = "https://ginger.activecloudediting.com/back/ace/action/upload/";


const isIOS = Platform.OS === 'ios';


function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

const CloseButton = (props) => {
  return <TouchableHighlight
    onPress={() => props.action && props.action()}>
    <FontAwesome
      name='close'
      size={20}
      style={{top: -25, right: 20, textAlign: 'right',}}

    />
  </TouchableHighlight>
}

const IOSAsyncMediaPicker = () => {
  return new Promise((resolve, reject) => {
    ImagePickerIOS.openSelectDialog({showVideos: false},
      (uri)=>  resolve({uri}),
      () => resolve({cancelled:true})
    )
  })
}

/*
  according to 'selectedProj != null' => display project or display list
*/
const ProjectSelector = (props) => {
  //const state = props.state;
  const dataSource = props.dataSource;
  console.log("ProjectSelector")
  console.log(props)
  console.log(props.selectedProjectId)
  let single = props.projects.length == 1;
  if (props.selectedProjectId != null || props.selectedProjectId != undefined) {
    return <View style={{}}>
      <Text style={styles.text}>
        Project {props.projectTitle}
      </Text>
      {!single &&
        <Button color="#637ba9" 
                useNativeControls="true"
                style={styles.customButton} 
                title="Unselect project" 
                onPress={() =>props.unselectProject()}/>
      }
    </View>
  }

  else {  
    return (
      <View style={styles.projectSelectionView}>
        <Text style={[styles.projectsList, styles.text]}> 
          Please select a Ginger project 
        </Text>
        <ListView
          style={styles.listView}
          dataSource={dataSource.cloneWithRows(props.projects)}
          renderRow={
            (project) =>
            //{project.id == props.selectedProjectId &&
              <Button color="#aaaaaa" 
                      style={styles.listViewButton} 
                      title={project.title}
                      onPress={() => props.selectProject(project)}/>
            //}
          }
        />
      </View>
    )
  }
}

export default class APITest extends React.Component {

  static navigationOptions = {
    title: 'Upload to Ginger',
  };

  constructor(props) {
    super(props);
    this.state = {
      mediaURI: null,
      mediaType: null,
      mediaName: null,
      token: null,
      projects: [],
      //projects: [/*{id:115,title:"test 1"},{id:115,title:"test 2"}*/],
      selectedProjectId: null,
      projectTitle: "Please select a project",
      isVisible: false,
      loginFormValues: null,

      isGettingToken: true,
      isGettingProjects: true,
      isLoginError: false,
      isUploaded: false,
      isUploading: false,
      username: '',
      password: '',
    };
    this.dataSource = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
  }

  setStateAsync(state) {
    return new Promise((resolve) => this.setState(state, resolve));
  }

  async componentWillMount() {
    //console.log('component will mount');
    //Expo.ScreenOrientation.allow(Expo.ScreenOrientation.Orientation.PORTRAIT);
    try {
      var promiseToken = this.getToken();
    } catch (e) {
      this.setState({token: null});
    }

    promiseToken.then(token => {
      //console.log('token on load : ' + token);
      if (token != "null") {
        this.setState({token: token});
      }
      this.getProjectsFromApi();
    }).catch(function (error) {
        console.log('There has been a problem for getting projects from API ' + error.message);
        throw error;
    });
  }

  async componentWillUnmount() {
    //Expo.ScreenOrientation.allow(Expo.ScreenOrientation.Orientation.LANDSCAPE);
  }

  /*******************************
   * STATUS HANDLING
   */
  checkLoginStatus(response) {
    if (response.ok) {
      //console.log("LOGIN OK");
      this.setState({isLoginError: false});

      return response;
    } else {
      console.log('pw or login error');
      let error = new Error(response.statusText);
      error.response = response;
      this.setState({
        loginFormValues: null,
        isGettingToken: false,
        isLoginError: true
      });
      switch (response.status) {
        case 400:
          Alert.alert("Wrong Password or Username");
          break;
        case 500:
          Alert.alert("Wrong Password or Username");
          break;
        default:
          Alert.alert("Authentification error");
          break;
      }
      throw error;
    }
    return response;
  }

  checkProjectsStatus(response) {
    if (response.ok) {
      //console.log("PROJECTS OK");
      return response;
    } else {
      let error = new Error(response.statusText);
      error.response = response;
      console.log('checkProjectsStatus error');
      console.log(response);

      this.setState({
        isGettingToken: false,
        isGettingProjects: false,
      });

      switch (response.status) {
        case 400:
          console.log('400');
          break;
        case 500:
          console.log('Internal Server Error');
          break;
        default:
          console.log('PROJECTS LIST ERROR');
          break;
      }
      //return Promise.reject(error);
      //throw error;
    }
    return response;
  }

  checkUploadStatus(response) {
    if (response.ok) {
      //console.log("UPLOAD OK");
      Alert.alert("Successfully uploaded");
      return response;
    } else {
      let error = new Error(response.statusText);
      error.response = response;
      console.log('checkUploadStatus error');
      console.log(response);

      Alert.alert("Uploading error");

      this.setState({
        isUploaded: false,
        isUploading: false
      });
      switch (response.status) {
        case 400:
          console.log('400');
        case 404:
          console.log('404 not found');
          break;
        case 500:
          console.log('500');
          break;
        default:
          console.log('UPLOAD ERROR');
          break;
      }
      throw error;
    }
    return response;
  }

  /*******************************
   * GETTERS // SETTERS
   */
  setToken = async (token) => {
    await AsyncStorage.setItem('@token:key', token);
    this.setState({token: token});
  }

  getToken = async () => {
    //console.log("getToken");
    try {
      const value = await AsyncStorage.getItem('@token:key');
      if (value !== null || value != "null") {
        // We have data!!
        return value;
      }
    } catch (error) {
      console.log("getToken error");
    }
  }

  setProjects = async (projectsData) => {

    //console.log(" ********* setProjects ");
    let projectsArray = [];
    for (let project of projectsData) {
      projectsArray.push({title: project.title, id: project.id});
    }

    //console.log(projectsArray);
    if(projectsArray.length == 1) {
      this.setState({projects: projectsArray, selectedProjectId : projectsArray[0].id, projectTitle :projectsArray[0].title });
    }
    else {
      this.setState({projects: projectsArray});
    }
    

    await AsyncStorage.setItem('@projects:key', JSON.stringify(projectsData));
  }

  getProjects = async () => {
    try {
      const value = await AsyncStorage.getItem('@projects:key');
      if (value !== null) {
        return value;
      }
    } catch (error) {
      console.log("getProjects error");
    }
  }

  getProjectsList = async () => {
    try {
      let projects = [];
      return this.getProjects().then((data) => {
        for (let project of JSON.parse(data)) {
          projects.push({title: project.title, id: project.id});
        }

        //console.log(" **** PROJECTS  ======= ");
        //console.log(projects);
        return projects;
      })
        .catch(function (error) {
          console.log('There has been a problem with your fetch operation: ' + error.message);
          throw error;
        });
    } catch (error) {
      console.log("getProjectsList error");
      console.log(error);
    }
  }

  /*******************************
   * API CALLS
   */
  login() {
    let formdata = new FormData();
    if (this.state.username !='' && this.state.password !='') { // if validation fails, value will be null

      formdata.append("username", this.state.username);
      formdata.append("password", this.state.password);

      fetch(loginURL, {
        method: 'POST',
        headers: {
          'Content-Type': 'multipart/form-data',
        },
        body: formdata
      }).then(this.checkLoginStatus.bind(this))
        .then(response => response.json())
        .then(json => {
          try {
            this.setToken(json['token'])
              .then(this.getProjectsFromApi.bind(this))
              .catch(function (error) {
                console.log('There has been a problem with your fetch operation: ' + error.message);
                throw error;
              });
          } catch (error) {
            console.log("setToken error");
            console.log(error);
            this.setToken(null)
            throw error;
          }
        }).catch(function (error) {
        console.log('There has been a problem with your fetch operation: ' + error.message);
        throw error;
      });
    }else{
      alert('Please enter login informations');
    }
  }

  getProjectsFromApi() {
    console.log("getProjectsFromApi");
    try {
      this.getToken().then((token) => {
        console.log("token");
        console.log(token);
        if(token == null || token == "null" || token == "undefined" || token == undefined ){
          this.setState({
            isGettingToken: false,
            isGettingProjects: false,
          });
          return
        }
        fetch(projectsListURL, {
          method: 'GET',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + token,
            'Host': 'ginger.activecloudediting.com'
          }
        })
          .then(this.checkProjectsStatus.bind(this))
          .then(response => response.json())
          .then(json => {
            try {
              this.setProjects(json["results"])
                .then(function () {
                  //console.log("PROJECTS SETTED");
                })
                .catch(function (error) {
                  console.log('setProjects ==> There has been a problem with your fetch operation: ' + error.message);
                  throw error;
                });
            } catch (error) {
              console.log("getProjectsFromApi error");
              console.log(error);
            }
            this.setState({
              isGettingToken: false,
              isGettingProjects: false,
            });
          }).catch(function (error) {
          console.log('There has been a problem with your fetch operation: ' + error.message);
          throw error;
        });
      });
    } catch (error) {
      console.log("getToken error");
      console.log(error);
    }
  }

//Custom Function to upload a file.


  uploadMedia = () => {
    //console.log("uploadMedia",this.state.mediaURI);
    const mediaURI = this.getMediaURI();
    this.setState({
      isUploading: true
    });
    try {
      this.getToken().then((token) => {

        let formdata = new FormData();
        formdata.append("project_id", this.state.selectedProjectId);
        formdata.append("authToken", token);
        //let name = this.parseMediaName(mediaURI);
        console.log("mediaURI");
        console.log(mediaURI);
        console.log("selectedProjectId");
        console.log(this.state.selectedProjectId);
        
        if(this.state.mediaName == null || this.state.mediaName == undefined){
          let mediaName = this.parseMediaName(mediaURI);
          this.setState({  mediaName: mediaName });
        }
        if(this.state.mediaType == null || this.state.mediaType == undefined){
          let mediaType = this.parseMediaType(this.state.mediaName);
          this.setState({ mediaType: mediaType });
        }

        formdata.append('media', {
          uri: mediaURI,
          type: this.state.mediaType, // or photo.type
          name: this.state.mediaName
        });
        fetch(uploadMediaURL + this.state.selectedProjectId, {
          method: 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'multipart/form-data',
            'Authorization': 'Token ' + token
          },
          body: formdata
        })
          .then(this.checkUploadStatus.bind(this))
          .then(response => response.json())
          .then(json => {
            try {
              this.uploadFinished(json);
            } catch (error) {
              console.log("uploadMedia error");
              console.log(error);
            }
          }).catch(function (error) {
          console.log(' fetch uploadMediaURL There has been a problem with your fetch operation: ');
          console.log(error.message);
          console.log(error);
          throw error;
        });
      }).catch(function (error) {
        console.log('There has been a problem with your fetch operation: ' + error.message);
        throw error;
      });
    } catch (error) {
      console.log("getToken error");
      console.log(error);
    }
  }

  parseMediaName(URI) {
    let name = "test.mp4";
    //console.log(URI);

    try{
      if (URI.indexOf('ImagePicker') >= 0) {
        name = URI.split("ImagePicker/")[1];
      } else {
        name = URI.split("GingerMedias/")[1];
      }
    }catch(e){
      console.log("parseMediaName error");
      console.log(e);
    }

    //console.log("parseMediaName")
    //console.log(name)
    return name;
  }

  parseMediaType(name) {
    let type = "video/mp4";
    //console.log(name);
   // console.log("name");

    try{
      if (name.indexOf('.mp4') >= 0) {
        type = "video/mp4";
      } else if (name.indexOf('.png') >= 0) {
        type = "image/png";
      } else if ((name.indexOf('.jpeg') >= 0) ||(name.indexOf('.jpg') >= 0) || (name.indexOf('.JPG') >= 0) || (name.indexOf('.JPEG') >= 0)) {
        type = 'image/jpg';
      }else {
        type = "video/mp4";
      }
      //console.log("parseMediaType")
      //console.log(type)
    }catch(e){
      console.log("parseMediaType error");
      console.log(e);
    }

    return type;
  }

  uploadFinished(json) {
    //console.log("UPLOAD RESPONSE json");
    //console.log(json);
    this.setState({
      image: null,
      isUploaded: true,
      mediaURI:null,
      isUploading: false
    });
  }

  _pickImage = async () => {
    try {
      let mediaURI = null;
      let mediaName = null;
      let mediaType = null;

      let result = await ImagePicker.launchImageLibraryAsync({
        allowsEditing: true
      });

      //console.log(" **** ImagePicker **** result");

      //console.log(result);

        if (!result.cancelled) {
          mediaURI = result.uri;
          mediaName = result.name;
          mediaType = this.parseMediaType(mediaName);
        }
      }catch(e){
        console.log('_pickImage error occured', e);
        alert('error occured' + e.message);
      }
  };

  chooseFromGallery = async () => {
    try {
      let mediaURI = null;
      let mediaName = null;
      let mediaType = null;
      
      //console.log("chooseFromGallery");
      if (isIOS) {
        //const result =  await IOSAsyncMediaPicker();

        let result = await ImagePicker.launchImageLibraryAsync({ 
          allowsEditing: true,
          mediaTypes : "All"
        });
        if (!result.cancelled) {
          //console.log('got here');
          mediaURI = result.uri;
          mediaName = result.name;
          mediaType = this.parseMediaType(mediaName);
        }
        //console.log('selected mediaURI', mediaURI);
      } else {
        //await DocumentPicker.getDocumentAsync({ type: 'video/*' })
        //const result = await DocumentPicker.getDocumentAsync({});*/

        //console.log(" **** ImagePicker **** result");
        let result = await ImagePicker.launchImageLibraryAsync({
          allowsEditing: true,
          mediaTypes : "All"
        });


        if (!result.cancelled) {
          mediaURI = result.uri;
          mediaName = this.parseMediaName(mediaURI);
          mediaType = this.parseMediaType(mediaName);
        }
        //console.log(mediaURI);
        //console.log(mediaType);
      }

      await this.setStateAsync({mediaURI,mediaName,mediaType});

    } catch (e) {
      console.log('choose gallery error occured', e);
      alert('error occured' + e.message);
    }
  };

  setSelectedProject(project) {
    //console.log(" ******************** setselectedProject Id and title");
    //console.log(project);
    //console.log(project.id);
    this.setState({
      selectedProjectId: project.id,
      projectTitle: project.title
    });
    return true;
  }

  showPopover() {
    this.setState({isVisible: true});
  }

  closePopover() {
    this.setState({isVisible: false});
  }


  clearStorage = async () => {
    //console.log("getToken");
    try {
      const value = await AsyncStorage.getItem('@token:key');

      //this.setToken(null);
      //AsyncStorage.removeItem('token');
      await AsyncStorage.setItem('@token:key', "null");

      this.setState({
        mediaURI: null,
        mediaType: null,
        mediaName: null,
        token: null,
        projects: [],
        //projects: [/*{id:115,title:"test 1"},{id:115,title:"test 2"}*/],
        selectedProjectId: null,
        projectTitle: "Please select a project",
        isVisible: false,
        loginFormValues: null,

        isGettingToken: false,
        isGettingProjects: false,
        isLoginError: false,
        isUploaded: false,
        isUploading: false,
        username: '',
        password: ''
      });

      const hasToken = this.state.token !== null;
      console.log('has token : ', this.state.token, hasToken);

    } catch (error) {
      console.log("clearStorage error");
    }
  }



  getMediaURI = ()=>{
    if(this.state.mediaURI){
      return this.state.mediaURI;
    }else {
      const {navigation} = this.props;
      return  navigation.getParam('videoUri', null);
    }
  }

  //UNSELECT IS ALSO REDRESHING PROJECTS LIST
  unselectProject(){
    console.log("unselectProject");
      this.setState({
        selectedProjectId: null,
        projectTitle: "Please select a project"
      });
      this.getProjectsFromApi();
    return true;
  }

  renderLoginForm() {
    return (
      <View>
        <TextInput
          value={this.state.username}
          onChangeText={(username) => this.setState({ username })}
          placeholder={'Username'}
          style={styles.input}
          underlineColorAndroid="#aaaaaa"
        />
        <TextInput
          value={this.state.password}
          onChangeText={(password) => this.setState({ password })}
          placeholder={'Password'}
          secureTextEntry={true}
          style={styles.input}
          underlineColorAndroid="#aaaaaa"

        />
        {this.state.isLoginError &&
          <Text style={styles.text}>fail to log</Text>
        }
        <Button color="#637ba9" 
                style={styles.button} 
                title="Login" 
                onPress={this.login.bind(this)}
        />
      </View>
    );
  }

  renderProjectsList() {
    const {navigation} = this.props;
    const camUri = navigation.getParam('videoUri', null);
    const pickedUri = this.state.mediaURI;
    const hasUri = camUri || pickedUri;

    //const uri = hasUri && pickedUri ?  pickedUri : camUri;
    const mediaURI = hasUri && pickedUri ? pickedUri : camUri;
    const selectFromGalleryTitle = mediaURI ? "Choose another file from gallery" : "Choose from your gallery";
    const fileName = mediaURI && mediaURI.split('/').reverse()[0];
    const titleFilename = camUri?'captured video':'selected file'
    //if(camUri) this.state.mediaURI = camUri;

    if (this.state.isGettingProjects) {
      return <Text>Getting projects ...</Text>
    }
    else {
      const { width } = Dimensions.get('window');
      return (
        <ScrollView style={{flex: 1}}>

          <View style={{flex: 0.4}}>

            <ProjectSelector
              projects={this.state.projects}
              projectTitle={this.state.projectTitle}
              dataSource={this.dataSource}
              selectedProjectId={this.state.selectedProjectId}
              selectProject={this.setSelectedProject.bind(this)}
              unselectProject={this.unselectProject.bind(this)}
            />
          </View>

            {hasUri && isIOS &&
            
              <FontAwesome name="file-movie-o" size={64} color="green" style={{textAlign: 'center'}}/>
            }
            
          <View style={{flex: 0.2}}>
            <Text style={styles.text}> {titleFilename } : {fileName}</Text>
          </View>

          
          <View style={{flex: 0.2}}>

            <Button color="#637ba9" 
                    useNativeControls="true"
                    style={styles.customButton} 
                    title={selectFromGalleryTitle} 
                    onPress={this.chooseFromGallery}/>
          </View>

          {hasUri && 


          <View style={{ marginTop: 5}}>
          {/*
            this.state.mediaType != 'image/jpg' ?
            <Video
              source={{ uri:mediaURI }}
              useNativeControls={true}
              style={{ width : width, height: 300 }}
            />
            :
            <Image
              source={{ uri:mediaURI }}
              style={{ width : width, height: 300 }}
            />
          */}
            {!isIOS &&
            <Image
              source={{ uri:mediaURI }}
              style={{ width : width, height: 300 }}
            />
            }
            {hasUri && this.state.selectedProjectId && !this.state.isUploading && 
              <Button color="#637ba9" 
                      style={styles.customButton} 
                      title="Upload Media"
                      onPress={this.uploadMedia.bind(this)}
              />
            }
          </View>
          }

          {this.state.isUploading &&
            <View>
              <Text style={[styles.uploading, styles.text]}>
                Media is uploading ... 
              </Text>
            </View>
          }
        
          <View style={{flex: 0.1,marginTop: 15}}>
            <Button color="#637ba9" 
                    style={[styles.customButton, styles.logout]} 
                    title="LOG OUT"
                    onPress={this.clearStorage.bind(this)}
            />
          </View>
        </ScrollView>

      )
    }
  }


  render() {
    const {navigate} = this.props.navigation;
    const {navigation} = this.props;

    //LinkPreview.getPreview('https://www.youtube.com/watch?v=MejbOFk7H6c')      .then(data => console.debug(data));

    const hasToken = this.state.token !== null;
    console.log('has token : ', this.state.token, hasToken);
    return (
      <View style={styles.container}>
        {this.state.isGettingToken &&

            <Text style={styles.text}>Check user logged ...</Text>
        }

        {hasToken &&
            this.renderProjectsList()
        }

        {!hasToken &&
            this.renderLoginForm()
        }
      </View>
    );
  }
}
// justify-content: flex-start | flex-end | center | space-between | space-around | space-evenly

const styles = StyleSheet.create({
  input:{
    margin: 10,
    padding: 10,
    color: "#dddddd",
  },
  flex: {
    flex: 0.5,
  },
  uploadView: {
    flex: 1,
  },
  selectionView: {
    flex: 0.9,
    justifyContent: 'flex-start',
  },
  projectSelectionView: {
    flex: 0.5,
    justifyContent: 'flex-start',
  },
  mediaSelectionView: {
    flex: 0.5,
    justifyContent: 'flex-start',
  },
  logoutView: {
    flex: 0.1,
    justifyContent: 'flex-end',
  },
  normal: {
    flex: 1,
    color: 'black',
    margin: 10,
    padding: 5,
  },

  container: {
    flex: 1,
    margin: 0,
    padding: 5,
    backgroundColor: '#333333',
    flexDirection: 'column',
    justifyContent: 'space-between',

  },
  customButton: {
    height: 40,
    marginTop: 5,
    marginBottom: 5,
    color: '#637ba9',
  },
  logout: {
    justifyContent: 'flex-end',
  },
  uploading: {
    textAlign: 'center', // 
    fontWeight: 'bold',
    fontSize: 15,
  },
  projectsList: {
    textAlign: 'center', // 
  },
  projectSelected: {
    textAlign: 'center', // 
    fontSize: 19,
    marginTop: 5,
    marginBottom: 5,
  },
  text: {
    textAlign: 'center', // 
    fontWeight: 'bold',
    fontSize: 15,
    height: 35,
    lineHeight: 35,
    marginTop: 5,
   // backgroundColor: 'black',
    color: "white"
  },

  listView: {
    margin: 5,
    padding: 5,
    backgroundColor : '#eeeeee',
  },
  listViewButton: {
    color: '#dddddd'
  },
  listSeparator: {
    padding: 5,
  }
});
