import React from 'react';
import {Platform, Text, View, Card, Button, StyleSheet,AsyncStorage, Image, TouchableHighlight, ListView,Alert } from 'react-native';
import t from 'tcomb-form-native'; // 0.6.11
import { ImagePicker } from 'expo';
import bootstrap from 'tcomb-form-native/lib/stylesheets/bootstrap.js';

//import FileUploader from 'react-native-file-uploader'
import FileUpload from 'react-native-upload-file'

var options = {
  stylesheet : bootstrap
};

options.stylesheet.textbox.normal = {
  color: '#fff',
  height: 36,
  padding: 7,
  borderRadius: 4,
  borderColor: '#fff',
  borderWidth: 1,
  marginBottom: 5
};
options.stylesheet.controlLabel.normal = {
  color: '#fff',
  height: 36,
  padding: 7,
  borderRadius: 4,
  //borderWidth: 1,
  marginBottom: 5
};
const loginURL = "https://ginger.activecloudediting.com/back/user/api-token-auth/";
const projectsListURL = "https://ginger.activecloudediting.com/back/ace/api/projects/";
const uploadMediaURL = "https://ginger.activecloudediting.com/back/ace/action/upload/";

const isIOS = Platform.OS === 'ios';
// Form
const Form = t.form.Form;
// Form model
const User = t.struct({
  username: t.String,
  password: t.String,
});

export default class APITest extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      mediaURI: null,
      mediaType: null,
      token: null,
      projects: [/*{id:115,title:"test 1"},{id:115,title:"test 2"}*/],
      //projects: ['row 1', 'row 2'],
      selectedProject: null,
      projectTitle: "Please select a project",
      isVisible: false,
      loginFormValues : null,

      isGettingToken : true,
      isGettingProjects : true,
      isLoginError : false,
      isUploaded: false,
      isUploading : false
    };
    this.dataSource = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
  }

  async componentWillMount() {
    console.log('component will mount');
    Expo.ScreenOrientation.allow(Expo.ScreenOrientation.Orientation.PORTRAIT);
    try {
      var promiseToken = this.getToken();
    } catch (e) {
      this.setState({token : null});
    }
    promiseToken.then(token => {
      console.log('token on load : ' + token);
      if(token != "null") {
        this.setState({token : token});
      }
      // We use getProjectsFromApi to test the token here
        this.getProjectsFromApi();
    });
  }

  async componentWillUnmount() {
    Expo.ScreenOrientation.allow(Expo.ScreenOrientation.Orientation.LANDSCAPE);
  }

/*******************************
* STATUS HANDLING
*/
  checkLoginStatus (response) {
    if (response.ok) {
        console.log("LOGIN OK");
       this.setState({isLoginError : false});

      return response;
    } else {
      console.log('pw or login error');
      let error = new Error(response.statusText);
      error.response = response;
      this.setState({
        loginFormValues : null, 
        isGettingToken : false, 
        isLoginError : true
      });
      switch (response.status) {
        case 400:
          Alert.alert("Wrong Password or Username");
          break;
        case 500:
          Alert.alert("Wrong Password or Username");
          break;
        default:
          Alert.alert("Authentification error");
          break;
      }
      throw error;
    }
    return response;
  }
  checkProjectsStatus (response) {
    if (response.ok) {
      console.log("PROJECTS OK");
      return response;
    } else {
      let error = new Error(response.statusText);
      error.response = response;
      console.log('checkProjectsStatus error');
      console.log(response);

      this.setState({
        isGettingToken : false,
        isGettingProjects : false,
      });

      switch (response.status) {
        case 400:
          console.log('400');
          break;
        case 500:
          console.log('Internal Server Error');
          break;
        default:
          console.log('PROJECTS LIST ERROR');
          break;
      }
      //return Promise.reject(error);
      //throw error;
    }
    return response;
  }

  checkUploadStatus (response) {
    if (response.ok) {
      console.log("UPLOAD OK");
      Alert.alert("Successfully uploaded");
      return response;
    } else {
      let error = new Error(response.statusText);
      error.response = response;
      console.log('checkUploadStatus error');
      console.log(response);

      Alert.alert("Uploading error");

     this.setState({ 
       isUploaded: false,
       isUploading: false
     });
      switch (response.status) {
        case 400:
          console.log('400');
        case 404:
          console.log('404 not found');
          break;
        case 500:
          console.log('500');
          break;
        default:
          console.log('UPLOAD ERROR');
          break;
      }
      throw error;
    }
    return response;
  }

/*******************************
* GETTERS // SETTERS
*/
  setToken = async (token) => {
    await AsyncStorage.setItem('@token:key', token);
    this.setState({ token: token });
  }

  getToken = async () => {
    //console.log("getToken");
    try {
      const value = await AsyncStorage.getItem('@token:key');
      if (value !== null || value != "null" ){
        // We have data!!
        return value;
      }
    } catch (error) {
      console.log("getToken error");
    }
  }

  setProjects = async (projectsData) => {

    console.log(" ********* setProjects ");
    let projectsArray = [];
    for (let project of projectsData) {
        projectsArray.push({title:project.title,id:project.id});
    }

    console.log(projectsArray);
    this.setState({ projects: projectsArray });

    await AsyncStorage.setItem('@projects:key', JSON.stringify(projectsData));
  }

  getProjects = async () => {
    try {
      const value = await AsyncStorage.getItem('@projects:key');
      if (value !== null){
        return value;
      }
    } catch (error) {
      console.log("getProjects error");
    }
  }

  getProjectsList = async () => {
    try {
      let projects = [];
      return this.getProjects().then((data) => {
        for (let project of JSON.parse(data)) {
            projects.push({title:project.title,id:project.id});
        }

        console.log(" **** PROJECTS  ======= ");
        console.log(projects);
        return projects;
      })
      .catch(function(error) {
        console.log('There has been a problem with your fetch operation: ' + error.message);
        throw error;
      });
    }catch (error) {
      console.log("getProjectsList error");
      console.log(error);
    }
  }

/*******************************
* API CALLS
*/
  login() {
    let formdata = new FormData();
    var value = this.loginform.getValue();

    if (value) { // if validation fails, value will be null

      formdata.append("username", value.username);
      formdata.append("password", value.password);

      fetch(loginURL, {
        method: 'POST',
        headers: {
          'Content-Type': 'multipart/form-data',
        },
        body: formdata
      }).then(this.checkLoginStatus.bind(this))
      .then(response => response.json())
      .then(json => {
         try {
              this.setToken(json['token'])
              .then(this.getProjectsFromApi.bind(this))
              .catch(function(error) {
              console.log('There has been a problem with your fetch operation: ' + error.message);
                throw error;
              });
        } catch (error) {
          console.log("setToken error");
          console.log(error);
          this.setToken(null)
          throw error;
        }
      }).catch(function(error) {
        console.log('There has been a problem with your fetch operation: ' + error.message);
        throw error;
      });
    }
  }
  
  getProjectsFromApi(){
    try {
      this.getToken().then((token) => {
        fetch(projectsListURL, {
            method: 'GET',
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json',
              'Authorization': 'Bearer ' + token,
              'Host': 'ginger.activecloudediting.com'
            }
          })
          .then(this.checkProjectsStatus.bind(this))
          .then(response => response.json())
          .then(json => {
             try {
              this.setProjects(json["results"])
              .then(function(){
                console.log("PROJECTS SETTED");
              })
              .catch(function(error) {
                console.log('setProjects ==> There has been a problem with your fetch operation: ' + error.message);
                throw error;
              });
            } catch (error) {
              console.log("getProjectsFromApi error");
              console.log(error);
            }
            this.setState({
              isGettingToken : false,
              isGettingProjects : false,
            });
          }).catch(function(error) {
            console.log('There has been a problem with your fetch operation: ' + error.message);
            throw error;
          });
      });
    } catch (error) {
      console.log("getToken error");
      console.log(error);
    }
  }
//Custom Function to upload a file.


  uploadMedia(){
    console.log("uploadMedia");
    this.setState({ 
     isUploading: true
    });
    try {
      this.getToken().then((token) => {

        let formdata = new FormData();
        let mediaType = "video/mp4";
        if(this.state.mediaType == "image") mediaType = "image/jpeg"; // image/png
        formdata.append("project_id", this.state.selectedProject);
        formdata.append("authToken", token);
        let name = this.parseMediaName(this.state.mediaURI);
        formdata.append('media', {
          uri: this.state.mediaURI,
          type: mediaType, // or photo.type
          name: name
        });
        fetch(uploadMediaURL+this.state.selectedProject, {
            method: 'POST',
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'multipart/form-data',
              'Authorization': 'Token ' + token
            },
            body: formdata
          })
          .then(this.checkUploadStatus.bind(this))
          .then(response => response.json())
          .then(json => {
             try {
               this.uploadFinished(json);
            } catch (error) {
              console.log("uploadMedia error");
              console.log(error);
            }
          }).catch(function(error) {
            console.log(' fetch uploadMediaURL There has been a problem with your fetch operation: ');
            console.log(error.message);
            console.log(error);
            throw error;
          });
      }).catch(function(error) {
          console.log('There has been a problem with your fetch operation: ' + error.message);
          throw error;
      });
    } catch (error) {
      console.log("getToken error");
      console.log(error);
    }
  }

  parseMediaName(URI){
    let name = "test.mp4";
    console.log(URI);
    if( URI.indexOf('ImagePicker') >= 0){
      name = URI.split("ImagePicker/")[1];
    }else{
      name = URI.split("GingerMedias/")[1];
    }
    console.log("parseMediaName")
    console.log(name)
    return name;
  }

  uploadFinished(json){
   console.log("UPLOAD RESPONSE json");
   console.log(json);
   this.setState({ 
     image: null,
     isUploaded: true,
     isUploading: false
   });
  }

  _pickImage = async () => {
    try {
      let mediaURI = null;
      let mediaName = null;
      let mediaType = null;
      
        console.log("chooseFromGallery");
      if (isIOS) {
        //const result =  await IOSAsyncMediaPicker();

        let result = await ImagePicker.launchImageLibraryAsync({ allowsEditing: true, });
        if (!result.cancelled) {
          console.log('got here');
          mediaURI = result.uri;
          mediaName = result.name;
          mediaType = this.parseMediaType(mediaName);
        }
        await this.setStateAsync({mediaURI,mediaName,mediaType});

        console.log('selected mediaURI', mediaURI);
      } else {

        let result = await ImagePicker.launchImageLibraryAsync({
          allowsEditing: true
        });

        console.log(" **** ImagePicker **** result");

        console.log(result);

        if (!result.cancelled) {
          this.setState({ 
            mediaURI: result.uri,
            mediaType: result.type,
            mediaName: result.name
           });
        }
      }
    } catch (e) {
      console.log('choose gallery error occured', e);
      alert('error occured' + e.message);
    }
  };

  setSelectedProject(project){
    this.setState({ 
      selectedProject: project.id, 
      projectTitle: project.title 
    });

  }
  showPopover() {
    this.setState({isVisible: true});
  }

  closePopover() {
    this.setState({isVisible: false});
  }


  clearStorage = async () => {
    //console.log("getToken");
    try {
      const value = await AsyncStorage.getItem('@token:key');
  
      //this.setToken(null);
      //AsyncStorage.removeItem('token');
      await AsyncStorage.setItem('@token:key', "null");
      this.setState( {token : null} );
    }catch (error) {
      console.log("clearStorage error");
    }
  }

  renderLoginForm() {
    return (
      <View>
        <Form inputStyle={{color : '#dddddd'}} value={this.state.loginFormValues} ref={c => this.loginform = c} type={User} />
        {this.state.isLoginError &&
          <Text style = {styles.text}>fail to log</Text>
        }
        <Button  color ='#637ba9' style={styles.button} title="Login" onPress={this.login.bind(this)} />
      </View>
    );
  }

  renderProjectsList() {
    const { navigation } = this.props;
    const camUri = navigation.getParam('videoUri', null);
    
    const pickedUri = this.state.mediaURI;
    const hasUri = camUri || pickedUri;

    console.log("isIOS");
    console.log(isIOS);
    //const uri = hasUri && pickedUri ?  pickedUri : camUri;
    this.state.mediaURI = hasUri && pickedUri ?  pickedUri : camUri;
    //if(camUri) this.state.mediaURI = camUri;

    if(this.state.isGettingProjects) {
      return <Text  style = {styles.text}>Getting projects ...</Text>
    }
    else {
      return(
        <View>
          <View>
            <View>
              <Text> Select your project </Text>  
              <ListView
                style={styles.listView}
                dataSource={this.dataSource.cloneWithRows(this.state.projects)}
                renderRow={
                  (project) =>  
                  <Button style={styles.customButton} title={project.title} onPress={this.setSelectedProject.bind(this,project)}  />

                }
              />

              {this.state.selectedProject &&
                <Text style={styles.text} > Selected project </Text> 
              }
              {this.state.selectedProject &&
                <Text style={styles.text} >{this.state.projectTitle}</Text>  
              }
            </View>

            <View>
              
                <Button color ='#637ba9' style={styles.button} title="Pick a media from your files" onPress={this._pickImage}  />
              
              {hasUri && 
                <Image source={{isStatic:true, uri: this.state.mediaURI }} style={{ width: 450, height: 300 }} />
              }
              {isIOS && camUri &&
                <Text style={styles.text} > Your video has been captured correctly, you can upload it. </Text> 
              }
              {hasUri && this.state.selectedProject && !this.state.isUploading &&
                <Button color="#54f286" color ='#637ba9'  style={[styles.button, styles.uploadButton]} title="Upload Media" onPress={this.uploadMedia.bind(this)} />
              }
              {this.state.isUploading && 
                <Text style={ styles.text} >Media is uploading ... </Text>  
              }
            </View>
          </View>
          <View>
            <Button style={styles.button} title="LOG OUT" onPress={this.clearStorage.bind(this)} />
          </View>
        </View>
      )
    }
  }


  render() {
    const { navigate } = this.props.navigation;
    const { navigation } = this.props;

    const hasToken =  this.state.token !== null;
    console.log('has token : ', this.state.token, hasToken);
    return (
      <View style={styles.container}>
        {
          this.state.isGettingToken ? 
            
            <Text>Check user logged ...</Text>
            
          : hasToken ?
              this.renderProjectsList()
            :
              this.renderLoginForm()
        }
      </View>
    );
  }
}
// justify-content: flex-start | flex-end | center | space-between | space-around | space-evenly

// const styles = StyleSheet.create({
//   uploadView:{
//     flex: 1,
//   },
//   selectionView:{
//     flex: 0.9,
//     justifyContent: 'flex-start',
//   },
//   projectSelectionView:{
//     flex: 0.5,
//     justifyContent: 'flex-start',
//   },
//   mediaSelectionView:{
//     flex: 0.5,
//     justifyContent: 'flex-start',
//   },
//   logoutView:{
//     flex: 0.1,
//     justifyContent: 'flex-end',
//   },


//   container: {
//     flex: 1,
//     margin: 10,
//     padding: 5,
//     backgroundColor: '#ffffff',
//     flexDirection: 'column',
//     justifyContent: 'space-between',
       
//   },
//   customButton: {
//     height:40,
//     marginTop: 5,
//     marginBottom: 5,
//   },
//   logout: {
//     justifyContent: 'flex-end',
//   },
//   uploading: {
//     textAlign: 'center', // 
//     fontWeight: 'bold',
//     fontSize: 15,
//   },
//   projectsList: {
//     textAlign: 'center', // 
//   },
//   projectSelected: {
//     textAlign: 'center', // 
//     fontSize: 19,
//     marginTop: 5,
//     marginBottom: 5,
//   },
//   text: {   
//     textAlign: 'center', // 
//     fontWeight: 'bold',
//     fontSize: 15,
//     height:35,
//     lineHeight: 35,
//     marginTop: 5,
//     backgroundColor: 'black',
//     color:"white"
//   },

//   listView:{
//     margin: 5,
//     padding: 5,
//   },
//   listSeparator:{
//     padding: 5,
//   }
// });

const styles = StyleSheet.create({
  container: {
    margin: 0,
    padding: 10,
    backgroundColor: '#333333',
    color: 'white',
    flex: 1,
  },
  button: {
    margin: 10,
    marginTop: 10,
    padding: 5,
    color : '#637ba9',
  },
  listView:{
    margin: 10,
    padding: 5,
  },
  listSeparator:{
    padding: 5,
  },
  text: {
    color: '#dddddd'
  }

});