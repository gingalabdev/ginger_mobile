import React from 'react';

import {Platform, StyleSheet, Dimensions, Text, View, TouchableOpacity, Slider, Vibration, AsyncStorage, Button, NetInfo} from 'react-native';
import {Constants, Camera, FileSystem, Permissions, MediaLibrary} from 'expo';
import GalleryScreen from './GalleryScreen';
import isIPhoneX from 'react-native-is-iphonex';
import { Ionicons,FontAwesome,MaterialCommunityIcons } from '@expo/vector-icons';

const landmarkSize = 2;

//const MEDIAS_DIRECTORY = FileSystem.documentDirectory + "/";
const MEDIAS_DIRECTORY = FileSystem.documentDirectory + "GingerMedias/";

const flashModeOrder = {
  off: 'on',
  on: 'auto',
  auto: 'torch',
  torch: 'off',
};

const wbOrder = {
  auto: 'sunny',
  sunny: 'cloudy',
  cloudy: 'shadow',
  shadow: 'fluorescent',
  fluorescent: 'incandescent',
  incandescent: 'auto',
};


export default class CameraScreen extends React.Component {
  state = {
    flash: 'off',
    zoom: 0,
    autoFocus: 'on',
    depth: 0,
    type: 'back',
    whiteBalance: 'auto',
    ratio: '16:9',
    ratios: [],
    photoId: 1,
    movieId: 1,
    duration: 0,
    timer: null,
    showGallery: false,
    photos: [],
    faces: [],
    permissionsGranted: false,
    isConnected: true,
    shloudDisplayPostButton : false,
    isLandscape:null
  };
  static navigationOptions = {
    title: 'Camera',
  };

  constructor(props) {
    super(props);
    getCur = this.getCurVideoUri.bind(this);

  }

  async componentWillMount() {
    const cam = await Permissions.askAsync(Permissions.CAMERA);
    const audio = await Permissions.askAsync(Permissions.AUDIO_RECORDING);
    const camera_roll = await Permissions.askAsync(Permissions.CAMERA_ROLL);
    console.log('cam', cam, 'audio', audio);

    Expo.ScreenOrientation.allow(Expo.ScreenOrientation.Orientation.ALL);

    this.setState({isLandscape: this.isLandscape(Dimensions.get('window')) });
    Dimensions.addEventListener('change',function(event){
      this.setState({isLandscape: this.isLandscape(Dimensions.get('window')) });
    }.bind(this));

    this.setState({permissionsGranted: (cam.status === 'granted' && audio.status === 'granted')});
    try {

      const dir = FileSystem.documentDirectory + 'GingerMedias';
      const dirInfo = await FileSystem.getInfoAsync(dir);
      if (dirInfo.exists && dirInfo.isDirectory) {
        console.log('Directory exists');  
      }
      else {
        await FileSystem.makeDirectoryAsync(dir);  
      }
      
      var isConnected;
      NetInfo.getConnectionInfo().then((connectionInfo) => {
        console.log('Initial, type: ' + connectionInfo.type + ', effectiveType: ' + connectionInfo.effectiveType);
        if(connectionInfo.type == 'wifi' || connectionInfo.type == 'cellular' || Platform.OS === 'ios') {
           console.log('connected');
           isConnected = true;
        }
        else {  
          console.log('not connected');
          isConnected = false;        
        }
        this.setState({isConnected : isConnected});
      });

      NetInfo.addEventListener(
        'connectionChange',
        this.handleFirstConnectivityChange
      );
    }
    catch(e) {
      console.log(e);
    }
  }

  componentDidMount() {
    /*const dispatchConnected = isConnected => this.props.dispatch(this.setIsConnected(isConnected));

    NetInfo.isConnected.fetch().then().done(() => {
      NetInfo.isConnected.addEventListener('connectionChange', dispatchConnected);
    });*/
  }

  isLandscape(dim){
     console.log(dim);
   if(dim['height']< dim['width']){
     console.log("IS LANDSCAPE TRUE");
      return true;
    }else{
      return false;
    }
  }

  setIsConnected(isConnected){
    this.setState({isConnected : isConnected});
  }

  handleFirstConnectivityChange(connectionInfo) {
    console.log('First change, type: ' + connectionInfo.type + ', effectiveType: ' + connectionInfo.effectiveType);
    NetInfo.removeEventListener(
      'connectionChange',
      this.handleFirstConnectivityChange
    );
  }

  getRatios = async () => {
    const ratios = await this.camera.getSupportedRatios();
    return ratios;
  };

  toggleView() {
    this.setState({
      showGallery: !this.state.showGallery,
    });
  }

  toggleFacing() {
    this.setState({
      type: this.state.type === 'back' ? 'front' : 'back',
    });
  }

  toggleFlash() {
    this.setState({
      flash: flashModeOrder[this.state.flash],
    });
  }

  setRatio(ratio) {
    this.setState({
      ratio,
    });
  }

  toggleWB() {
    this.setState({
      whiteBalance: wbOrder[this.state.whiteBalance],
    });
  }

  toggleFocus() {
    this.setState({
      autoFocus: this.state.autoFocus === 'on' ? 'off' : 'on',
    });
  }

  zoomOut() {
    this.setState({
      zoom: this.state.zoom - 0.1 < 0 ? 0 : this.state.zoom - 0.1,
    });
  }

  zoomIn() {
    this.setState({
      zoom: this.state.zoom + 0.1 > 1 ? 1 : this.state.zoom + 0.1,
    });
  }

  setFocusDepth(depth) {
    this.setState({
      depth,
    });
  }

  saveToGallery = async (uri) => {
    try {
      const {status} = await Permissions.askAsync(Permissions.CAMERA_ROLL);

      if (status !== 'granted') {
        console.log('gallery not granted')
        throw new Error('Denied CAMERA_ROLL permissions!');
      }
      let tmp = new Date();
      var newDate = tmp.getHours.toString() + tmp.getMinutes.toString() + tmp.getSeconds.toString() + "_" + tmp.getDay().toString() + "-" + tmp.getMonth().toString() + "-" + tmp.getYear().toString();
       
      //const path = MEDIAS_DIRECTORY + newDate+`.mp4`;
      await MediaLibrary.createAssetAsync(uri);

      alert('Successfully saved photos to user\'s gallery!');

    } catch (e) {
      console.log(e)
      alert('error occurred! '+e.message);
    }
  };


  startRecording0 = async function () {
    console.log('start recording0');
    if (this.camera) {
      try {
        this.setState({isRecording: true});
        const timer = setInterval(() => {
          this.setState({duration: this.state.duration + 100});
        }, 100);

        this.setState({timer, duration: 0});
        try {
          const file = await this.camera.recordAsync({quality: '4:3'});
          const path = MEDIAS_DIRECTORY + `Movie_${this.state.movieId}.mp4`;
          this.setState({videoUri: file.uri});
          this.path = path;
          console.log('recording finished saving from', file.uri);
          await FileSystem.moveAsync({
            from: file.uri,
            to: path,
          });
          this.setState({movieId: this.state.movieId + 1,});
          await this.saveToGallery();
          Vibration.vibrate();
        } catch (error) {
          console.log('There has been a problem with your fetch operation: ' + error.message);
          throw error;
        }
        ;
      } catch (e) {
        console.log('error in startRecording', e)
      }
    }
  };

  //ensureDirAsync = async function (dir, intermediates=true) {
  ensureDirAsync = async (dir, intermediates=true) => {
      const props =  await FileSystem.getInfoAsync(dir)
      if( props.exists && props.isDirectory){
         return props;
      }
      await FileSystem.makeDirectoryAsync(dir, {intermediates})
      return await ensureDirAsync(dir, intermediates)
  }


  takePicture = async function () {
    if (this.camera) {
      this.camera.takePictureAsync()
      .then(async (data) => {

          let tmp = new Date();
          var newDate = tmp.getHours().toString() + tmp.getMinutes().toString() + tmp.getSeconds().toString() + "_" + tmp.getDay().toString() + "-" + tmp.getMonth().toString() + "-" + tmp.getYear().toString();
          const path = MEDIAS_DIRECTORY + newDate + `.jpg`;
          this.path = path;
          console.log('recording finished saving from', data.uri);


        await MediaLibrary.createAssetAsync(data.uri);

        FileSystem.moveAsync({
          from: data.uri,
          to: path,
        }).then(() => {
          this.setState({
            photoId: this.state.photoId + 1,
          });

          this.setState({ videoUri: path });
          Vibration.vibrate();
        });
      }) .catch(function(error) {
        console.log('There has been a problem with your fetch operation: ' + error.message);
          throw error;
        });
    }
  };

  startRecording = async function () {
    console.log('start recording');
    if (this.camera) {
      try {
        this.setState({isRecording: true});
        const timer = setInterval(() => {
          this.setState({duration: this.state.duration + 100});
        }, 100);

        this.setState({timer, duration: 0});
        this.camera.recordAsync({quality: '4:3'})
          .then(async (file) => {
            //isDirectory
            //const dir = await ensureDirAsync(MEDIAS_DIRECTORY);

            /*const props = FileSystem.getInfoAsync(MEDIAS_DIRECTORY)
            if(!props.exists){
              FileSystem.makeDirectoryAsync(MEDIAS_DIRECTORY)
            }*/

            let tmp = new Date();
            var newDate = tmp.getHours().toString() + tmp.getMinutes().toString() + tmp.getSeconds().toString() + "_" + tmp.getDay().toString() + "-" + tmp.getMonth().toString() + "-" + tmp.getYear().toString();
            const path = MEDIAS_DIRECTORY + newDate + `.mp4`;
            this.path = path;
            console.log('recording finished saving from', file.uri);

            
            //await this.saveToGallery();

            await MediaLibrary.createAssetAsync(file.uri);

            FileSystem.copyAsync({
              from: file.uri,
              to: path,
            }).then(() => {
              this.setState({
                movieId: this.state.movieId + 1,
              });

              this.setState({ videoUri: path });
              Vibration.vibrate();
            });


          })
          .catch(function(error) {
          console.log('There has been a problem with your fetch operation: ' + error.message);
            throw error;
          });
      } catch (e) {
        console.log('error in startRecording', e)
      }
    }
  };

  stopRecording() {
    console.log('try stop recording ');
    try {
      this.state.timer && clearInterval(this.state.timer);
      if (this.camera && this.state.isRecording) {
        console.log('calling cam.stopRecording');
        this.camera.stopRecording();

        this.setState({
          isRecording: false,
          shloudDisplayPostButton : true,
          hasVideo : true
        });
      } else {
        console.log('error this.state.isRecording', this.state.isRecording);
        console.log('error this.camera', this.camera);

      }
    } catch (e) {
      console.log('erreur stopping rec', e);
    }
  }

  onFacesDetected = ({faces}) => this.setState({faces});
  onFaceDetectionError = state => console.warn('Faces detection error:', state);

  

  getCurVideoUri() {
    console.log('getter');
    console.log(this.state.videoUri);
   return this.state.videoUri; 
  }
  

  render() {
    // {this.getTokenIfNot()}
    const { navigate } = this.props.navigation;
    const cameraScreenContent = this.state.permissionsGranted
      ? this.renderCamera()
      : this.renderNoPermissions();
    const content = this.state.showGallery ? this.renderGallery() : cameraScreenContent;
    return (<View style={styles.container}>
      {content}
      {(this.state.isConnected == true || this.state.hasVideo == true) && 
        <Button title="post/login"
                onPress = { () => {
                                   
                    console.log('on press');
                    this.props.navigation.navigate('Post',this.state.videoUri);

                  

                }}
        />

      }



    </View>);
  }



  renderGallery() {
    return <GalleryScreen onPress={this.toggleView.bind(this)}/>;
  }

  renderFace({bounds, faceID, rollAngle, yawAngle}) {
    return (
      <View
        key={faceID}
        transform={[
          {perspective: 600},
          {rotateZ: `${rollAngle.toFixed(0)}deg`},
          {rotateY: `${yawAngle.toFixed(0)}deg`},
        ]}
        style={[
          styles.face,
          {
            ...bounds.size,
            left: bounds.origin.x,
            top: bounds.origin.y,
          },
        ]}>
        <Text style={styles.faceText}>ID: {faceID}</Text>
        <Text style={styles.faceText}>rollAngle: {rollAngle.toFixed(0)}</Text>
        <Text style={styles.faceText}>yawAngle: {yawAngle.toFixed(0)}</Text>
      </View>
    );
  }

  renderLandmarksOfFace(face) {
    const renderLandmark = position =>
      position && (
        <View
          style={[
            styles.landmark,
            {
              left: position.x - landmarkSize / 2,
              top: position.y - landmarkSize / 2,
            },
          ]}
        />
      );
    return (
      <View key={`landmarks-${face.faceID}`}>
        {renderLandmark(face.leftEyePosition)}
        {renderLandmark(face.rightEyePosition)}
        {renderLandmark(face.leftEarPosition)}
        {renderLandmark(face.rightEarPosition)}
        {renderLandmark(face.leftCheekPosition)}
        {renderLandmark(face.rightCheekPosition)}
        {renderLandmark(face.leftMouthPosition)}
        {renderLandmark(face.mouthPosition)}
        {renderLandmark(face.rightMouthPosition)}
        {renderLandmark(face.noseBasePosition)}
        {renderLandmark(face.bottomMouthPosition)}
      </View>
    );
  }

  renderFaces() {
    return (
      <View style={styles.facesContainer} pointerEvents="none">
        {this.state.faces.map(this.renderFace)}
      </View>
    );
  }

  renderLandmarks() {
    return (
      <View style={styles.facesContainer} pointerEvents="none">
        {this.state.faces.map(this.renderLandmarksOfFace)}
      </View>
    );
  }

  renderNoPermissions() {
    return (
      <View style={{flex: 1, alignItems: 'center', justifyContent: 'center', padding: 10}}>
        <Text style={{color: 'white'}}>
          Camera permissions not granted - cannot open camera preview.
        </Text>
      </View>
    );
  }

  renderCamera() {
    const {width} = Dimensions.get('window');
    return (
      <Camera
        ref={ref => {
          this.camera = ref;
        }}
        style={{
          flex: 1,
        }}
        type={this.state.type}
        flashMode={this.state.flash}
        autoFocus={this.state.autoFocus}
        zoom={this.state.zoom}
        whiteBalance={this.state.whiteBalance}
        ratio={this.state.ratio}
        faceDetectionLandmarks={Camera.Constants.FaceDetection.Landmarks.all}
        onFacesDetected={this.onFacesDetected}
        onFaceDetectionError={this.onFaceDetectionError}
        focusDepth={this.state.depth}>
        <View
          style={{
            flex: 1,
            backgroundColor: 'transparent',
            flexDirection: 'column',
            justifyContent: 'space-between',
          }}>
          <View style={{flex: 1, flexDirection: 'column',alignSelf: 'flex-end'}}>
            <TouchableOpacity style={[styles.flipButton]}
                              onPress={this.toggleFacing.bind(this)}>
              <Ionicons name="ios-reverse-camera-outline" size={32} color="black"/>
            </TouchableOpacity>

            {!this.state.isRecording && this.state.isLandscape &&
            <TouchableOpacity
              style={[styles.flipButton, styles.picButton]}
              onPress={this.takePicture.bind(this)}>
              <FontAwesome name="picture-o" size={32} color="green"/>

            </TouchableOpacity>
            }
            {!this.state.isRecording && this.state.isLandscape && <TouchableOpacity
              style={[styles.flipButton, styles.galleryButton]}
              onPress={this.startRecording.bind(this)}>
              <MaterialCommunityIcons name="record-rec" size={32} color="red"/>

            </TouchableOpacity>
            }

            {this.state.isRecording && this.state.isLandscape && <TouchableOpacity
              style={[styles.flipButton, styles.galleryButton]}
              onPress={this.stopRecording.bind(this)}>
              <FontAwesome name="stop" size={32} color="black"/>
            </TouchableOpacity>
            }
            {this.state.isRecording && this.state.isLandscape &&
                <Text style={styles.flipText}> {this.state.duration}</Text>
            }
          </View>

            {!this.state.isLandscape && 
              <Text style={{textAlign: 'center'}}>
                Recording disabled for portrait 
              </Text>
            }
            {!this.state.isLandscape && 
              <Text style={{textAlign: 'center'}}>
                Please use landscape mode
              </Text>
            }
        </View>
        <View
          style={{
            flex: 0.4,
            backgroundColor: 'transparent',
            flexDirection: 'row',
            alignSelf: 'flex-end',
            marginBottom: -5,
          }}>
          {this.state.autoFocus !== 'on' ? (
            <Slider
              style={{width: 150, marginTop: 15, marginRight: 15, alignSelf: 'flex-end'}}
              onValueChange={this.setFocusDepth.bind(this)}
              step={0.1}
            />
          ) : null}
        </View>
        <View
          style={{
            flex: 0.1,
            paddingBottom: isIPhoneX ? 20 : 0,
            backgroundColor: 'transparent',
            flexDirection: 'row',
            alignSelf: 'flex-end',
          }}>
        </View>
        {this.renderFaces()}
        {this.renderLandmarks()}
      </Camera>
    );
  }

  render() {
    const cameraScreenContent = this.state.permissionsGranted
      ? this.renderCamera()
      : this.renderNoPermissions();
    const content = this.state.showGallery ? this.renderGallery() : cameraScreenContent;
    return <View style={styles.container}>
            {content}
            {this.state.isConnected && 
              <Button title="Upload" onPress={this.onGotoPost.bind(this)}/>}
          </View>;

  }

  onGotoPost() {
    
      this.props.navigation.navigate('Post', {
        videoUri : this.path
      });
      console.log('goto post');
  }

}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#000',
  },
  navigation: {
    flex: 1,
  },
  gallery: {
    flex: 1,
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  flipButton: {
    height: 42,
    width: 42,
    marginHorizontal: 1,
    marginBottom: 4,
    marginTop: 4,
    borderRadius: 8,
    borderColor: 'black',
    borderWidth: 1,
    padding: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor:"#ffffff"
  },
  flipText: {
    color: 'white',
    fontSize: 15,
  },
  item: {
    margin: 4,
    backgroundColor: 'indianred',
    height: 35,
    width: 80,
    borderRadius: 5,
    alignItems: 'center',
    justifyContent: 'center',
  },
  picButton: {
    backgroundColor:"#ffffff"
  },
  galleryButton: {
    backgroundColor:"#ffffff"
  },
  facesContainer: {
    position: 'absolute',
    bottom: 0,
    right: 0,
    left: 0,
    top: 0,
  },
  face: {
    padding: 10,
    borderWidth: 2,
    borderRadius: 2,
    position: 'absolute',
    borderColor: '#FFD700',
    justifyContent: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
  landmark: {
    width: landmarkSize,
    height: landmarkSize,
    position: 'absolute',
    backgroundColor: 'red',
  },
  faceText: {
    color: '#FFD700',
    fontWeight: 'bold',
    textAlign: 'center',
    margin: 10,
    backgroundColor: 'transparent',
  },
  row: {
    flexDirection: 'row',
  },
});