
import React from 'react';
import { createStackNavigator } from 'react-navigation'; // Version can be specified in package.json
import  CameraScreen  from './../screens/CameraScreen';
//import  APITestScreen  from './../screens/APITestScreen';
import  UploadScreen  from './../screens/UploadScreen';
const NavigationStack = createStackNavigator(
    /*Camera: {
    screen: CameraScreen,
    navigationOptions: ({ navigation }) => ({
      title: 'Ginger Camera'
    })
  },
    Upload: {
    screen: UploadScreen,
    navigationOptions: ({ navigation }) => ({
      title: 'Upload to Ginger'
    })
  },*/
  {
    Camera : CameraScreen,
    Post: UploadScreen
  },
  {
    initialRouteName: 'Camera',
  }
);

export default class App extends React.Component {
  render() {
    return <NavigationStack />;
  }
}