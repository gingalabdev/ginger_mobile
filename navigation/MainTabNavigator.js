import React from 'react';
import { Platform } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { createStackNavigator, createBottomTabNavigator } from 'react-navigation';

import TabBarIcon from '../components/TabBarIcon';
import GalleryScreen from '../screens/GalleryScreen';
import CameraScreen from '../screens/CameraScreen';
import SettingsScreen from '../screens/SettingsScreen';
//import APITestScreen from '../screens/APITestScreen';
import UploadScreen from '../screens/UploadScreen';
import PostFromCam from '../screens/postFromCam';

const HomeStack = createStackNavigator({
  Home: GalleryScreen,
});

HomeStack.navigationOptions = {
  tabBarLabel: 'Recent shots',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={
        Platform.OS === 'ios'
          ? `ios-images${focused ? '' : '-outline'}`
          : 'md-images'
      }
    />
  ),
};

const cameraLinksStack = createStackNavigator({
  Links: CameraScreen,
  Post:PostFromCam

});

const APITestLinksStack = createStackNavigator({
  Links: UploadScreen,
});

cameraLinksStack.navigationOptions = {
  tabBarLabel: 'Camera',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={Platform.OS === 'ios' ? `ios-camera${focused ? '' : '-outline'}` : 'md-camera'}
    />
  ),
};

const SettingsStack = createStackNavigator({
  Settings: SettingsScreen,
});

SettingsStack.navigationOptions = {
  tabBarLabel: 'Settings',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={Platform.OS === 'ios' ? `ios-options${focused ? '' : '-outline'}` : 'md-options'}
    />
  ),
};

export default createBottomTabNavigator({
  cameraLinksStack,
  HomeStack,
  SettingsStack,
  APITestLinksStack,
});
